# stage-2-es6-for-everyone

## Instalation

`npm install`

`npm run build`

`npm run start`

open http://localhost:8080/

# About
Created on the base of [bsa repository](https://github.com/binary-studio-academy/stage-2-es6-for-everyone).
Created by Bohdan Panashenko for [Binary Studio Academy](https://academy.binary-studio.com/ua/), 2020.
